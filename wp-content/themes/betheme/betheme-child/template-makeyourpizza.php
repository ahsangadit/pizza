<?php /* Template Name: Make Your Pizza */ ?>

<?php
get_header();
?>
<?php include("priceandtopping.php"); 

// print_r($meat_topping);

?>  


<html lang="en">
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <body>

			<div class="container price">
				

		    <div class="size-option size-medium" data-value="medium">
		    	<input type="radio" class="pizza-size" checked="checked" value="<?php echo $pizza_price['medium']['price'] ?>" 
		    	topping-price="<?php echo $pizza_price['medium']['topping_price'] ?>" name="pizza-size" id="medium-size">
				<label class="size-option-title"  for="medium-size"><?php echo $pizza_price['medium']["name"] ?></label>
			  <!--   <span class="size-option-description">8 slices</span> -->
		    </div>
			<div class="size-option size-large" data-value="large">
				<input type="radio" class="pizza-size" value="<?php echo $pizza_price['large']['price'] ?>" 
				topping-price="<?php echo $pizza_price['large']['topping_price'] ?>" name="pizza-size" id="large-size">
				<label class="size-option-title" for="large-size"><?php echo $pizza_price['large']["name"] ?></label>
			    <!-- <span class="size-option-description">10 slices</span> -->
		    </div>

		    <div class="size-option cart-price">
				<h2 class="cart-option-title">Total Price</h2>
			    <label class="carts-price"></label>
		    </div>
		
		
		</div>
	

		<div id="accordion">
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h5 class="mb-0">
		        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		         <h3> Choose Your Crust <h3>
		        </a>
		      </h5>
		    </div>

		    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
		      <div class="card-body">

		      	 <div class="size-option size-regular" data-value="regular">
		    	<input type="radio" class="crust-size" value="" name="crust-size" id="regular-size">
				<label class="crust-option-title" for="regular-size"><?php echo $pizza_crust["regular-title"] ?></label>
			  <!--   <span class="size-option-description">8 slices</span> -->
		    </div>
			<div class="size-option size-thin" data-value="thin">
				<input type="radio" class="crust-size" value="" name="crust-size" id="thin-size">
				<label class="crust-option-title" for="thin-size"><?php echo $pizza_crust["thin-title"] ?></label>
			    <!-- <span class="size-option-description">10 slices</span> -->
		    </div>
		    <div class="size-option size-thick" data-value="thick">
				<input type="radio" class="crust-size" value="" name="crust-size" id="thick-size">
				<label class="crust-option-title" for="thick-size"><?php echo $pizza_crust["thick-title"] ?></label>
			    <!-- <span class="size-option-description">10 slices</span> -->
		    </div>
		       
		      </div>
		    </div>
		  </div>
		  <div class="card">
		    <div class="card-header" id="headingThree">
		      <h5 class="mb-0">
		        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
		        <h3> Choose Your Toppings </h3>
		        </a>
		      </h5>
		    </div>
		    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
		      <div class="card-body">

				<ul class="nav nav-tabs nav-center" role="tablist">
					  <li class="nav-item">
					    <a class="nav-link active" href="#meats" role="tab" data-toggle="tab">MEATS</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#veggies" role="tab" data-toggle="tab">VEGGEIS</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#cheese" role="tab" data-toggle="tab">CHEESE</a>
					  </li>
				</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					  <div role="tabpanel" class="tab-pane fade in active" id="meats">
					  	<br>
					  	
					  	<?php
					  	$i=1; 
					  	 foreach($meat_topping as $meat){?>

 
					  	 <div class="container">
					  	   <input type="checkbox" class="meat-radio"  name="meat-size-<?php echo $i; ?>" id="<?php echo $meat['title'] ?>" 
					  	   data-extra="<?php echo $meat['extratopping'] ?>">
					  	   <label class="meat" for="<?php echo $meat['title']?>" >
					  	   	<h5 class="builder-selector-title"><?php echo $meat["title"] ?></h5>
					  	   	<div class="meat-buttons">
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="left-size-<?php echo $i; ?>">
					  	   	<label class="buying-size1 buying-size-checked" for="left-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="full-size-<?php echo $i; ?>">
					  	   		<label class="buying-size2 buying-size-checked"  for="full-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="right-size-<?php echo $i; ?>">
					  	   		<label class="buying-size3 buying-size-checked" for="right-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size double-size" value="" name="double-size-<?php echo $i; ?>" id="double-size-<?php echo $i; ?>" unchecked>
					  	   		<label class="buying-size4 checked-double-size" for="double-size-<?php echo $i; ?>"><span>X2</span></label>
	            			</div>
					  	   </label>
					  	</div>
					  	<?php $i++; ?>

					   <?php } ?>

					  </div>
					  <div role="tabpanel" class="tab-pane fade" id="veggies">

					  	<br>
					  	<?php
                           $i=1; 
					  	 foreach($veggie_topping as $veggie){?>

					  	 <div class="container">
					  	 	<input type="checkbox" class="veggie-radio"  name="veggie-size-<?php echo $i; ?>" id="<?php echo $veggie['title'] ?>" 
					  	   data-extra="<?php echo $veggie['extratopping'] ?>">
					  	   <label class="veggie" for="<?php echo $veggie['title']?>">
					  	   	<h5 class="builder-selector-title"><?php echo $veggie["title"] ?></h5>
					  	   	<div class="veggie-buttons">
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="vleft-size-<?php echo $i; ?>">
					  	   	<label class="buying-size1 buying-size-checked" for="vleft-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="vfull-size-<?php echo $i; ?>">
					  	   		<label class="buying-size2 buying-size-checked"  for="vfull-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="vright-size-<?php echo $i; ?>">
					  	   		<label class="buying-size3 buying-size-checked" for="vright-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size double-size" value="" name="double-size-<?php echo $i; ?>" id="vdouble-size-<?php echo $i; ?>" unchecked>
					  	   		<label class="buying-size4 checked-double-size" for="vdouble-size-<?php echo $i; ?>"><span>X2</span></label>
	            			</div>
                           

					  	   </label>
					  	</div>

					   <?php $i++ ?>

					   <?php } ?>
					  	

					  </div>
					  <div role="tabpanel" class="tab-pane fade" id="cheese">

					  	<br>
					  	<?php foreach($Cheese_topping as $cheese){?>

					  	 <div class="container">
					  	 	<input type="checkbox" class="cheese-radio"  name="cheese-size-<?php echo $i; ?>" id="<?php echo $cheese['title'] ?>" 
					  	   data-extra="<?php echo $cheese['extratopping'] ?>">
					  	   <label class="cheese" for="<?php echo $cheese['title']?>">
					  	   	<h5 class="builder-selector-title"><?php echo $cheese["title"] ?></h5>
					  	   	 <div class="cheese-buttons">
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="cleft-size-<?php echo $i; ?>">
					  	   	<label class="buying-size1 buying-size-checked" for="cleft-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="cfull-size-<?php echo $i; ?>">
					  	   		<label class="buying-size2 buying-size-checked"  for="cfull-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size" value="" name="buying-size-<?php echo $i; ?>" id="cright-size-<?php echo $i; ?>">
					  	   		<label class="buying-size3 buying-size-checked" for="cright-size-<?php echo $i; ?>"></label>
					  	   	<input type="radio" class="buying-size double-size" value="" name="double-size-<?php echo $i; ?>" id="cdouble-size-<?php echo $i; ?>" unchecked>
					  	   		<label class="buying-size4 checked-double-size" for="cdouble-size-<?php echo $i; ?>"><span>X2</span></label>
	            			</div>

					  	   </label>
					  	</div>

					   <?php } ?>
					  	

					  </div>
					</div>
		    
		      </div>
		    </div>
		  </div>
		</div>

   </body>
</html>


<script>

		(function(jQuery) {
		    jQuery(document).ready(function() {
		        custom_pizza_price(); // start the loop
		    });
		})(jQuery);



		jQuery('.meat-radio').click(function(){

		if(jQuery(this).hasClass('active')){

		  jQuery(this).removeClass('active')

		}else{

		  jQuery(this).addClass('active')

		}
		custom_pizza_price()
		});

		jQuery('.cheese-radio').click(function(){

		if(jQuery(this).hasClass('active')){

		  jQuery(this).removeClass('active')

		}else{

		  jQuery(this).addClass('active')

		}
		custom_pizza_price()
		});

		jQuery('.veggie-radio').click(function(){

		if(jQuery(this).hasClass('active')){

		  jQuery(this).removeClass('active')

		}else{

		  jQuery(this).addClass('active')

		}
		custom_pizza_price()
		});


	jQuery(".size-option").click(function() {
		custom_pizza_price()
    });


function custom_pizza_price(){
 var total_price = 0;
 var pizza_topping_select = jQuery('.meat-radio.active')
 var pizza_topping_select1 = jQuery('.cheese-radio.active')
 var pizza_topping_select2 = jQuery('.veggie-radio.active')
 //console.log(pizza_topping_select.length);
 var pizza_size = jQuery("input[name='pizza-size']:checked")
 var pizza_price = jQuery("input[name='pizza-size']:checked").val()
 var pizza_price = parseFloat(pizza_price)
 var topping_price = parseFloat(pizza_size.attr('topping-price'));

    total_price= pizza_price

 jQuery('.carts-price').html(' $'+total_price);	
      
    pizza_topping_select.each(function(i,e){
    var topping_select_item = jQuery(this)
		
		      if(topping_select_item.attr('data-extra')=='true'){
		        total_price+=topping_price+1
		      }
		        total_price+=topping_price;
	});
	               if(pizza_topping_select.length>0 ){
					    total_price-=topping_price
				    }
				


        pizza_topping_select1.each(function(i,e){
          var topping_select_item = jQuery(this)
		
		      if(topping_select_item.attr('data-extra')=='true'){
		        total_price+=topping_price+1
		      }
		        total_price+=topping_price;
		 });
                  if(pizza_topping_select1.length>0 ){
					    total_price-=topping_price
				    }
        			// jQuery('.carts-price').html(' $'+total_price.toFixed(2) );    


            pizza_topping_select2.each(function(i,e){
              var topping_select_item = jQuery(this)
		
		      if(topping_select_item.attr('data-extra')=='true'){
		        total_price+=topping_price+1
		      }
		        total_price+=topping_price;
		   });
				    if(pizza_topping_select2.length>0 ){
					    total_price-=topping_price
				    }

					 jQuery('.carts-price').html(' $'+total_price.toFixed(2) );    
					 console.log(total_price);
 }


</script>



<?php
get_footer();
?>