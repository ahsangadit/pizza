<?php

// $pizza_price = array(
// 	             "Medium-title"=>"Medium",
// 	             "Large-title"=>"Large",
// 	             "Medium-price"=>8.99, 
// 	             "Large-price"=>9.99,
// 	             "Medium Topping"=>1.5,
// 	             "Large Topping"=>1.75   
//              ); 

$pizza_price = [
                'large' =>[
                    'name' =>'LARGE',
                    'price'=>9.99,
                    'topping_price'=>'1.75'
                ],
                'medium' =>[
                    'name' =>'MEDIUM',
                    'price'=>8.99,
                    'topping_price'=>'1.50'
                ]
            ];


$pizza_crust = array(
	             "regular-title"=>"Regular",     
	             "thin-title"=>"Thin",
	             "thick-title"=>"Thick"
             );  


$meat_topping =
[
     
        [
        'title' => 'Pepperoni', 
        'extratopping' => 'false' 
       ],
       [
        "title" => "Italian Sausage", 
        "extratopping" => 'false' 
      ],
       [
        "title" => "Beef Bacon*", 
        "extratopping" => 'true' 
      ],
       [
        "title" => "Ground Beef*", 
        "extratopping" => 'true' 
      ],
       [
       	"title" => "Grilled Chicken", 
        "extratopping" => false 
      ], 
       [
        "title" => "Jerk Chicken*", 
        "extratopping" => 'true',
        "extradollar" => 'true' 
      ], 
       [
        "title" => "Tandoori chicken*", 
        "extratopping" => 'true',
        "extradollar"  => 'true' 
      ] 
];

$veggie_topping=array(
  
	    "BlackOlives" => array( 
        "title" => "Black Olives", 
        "extratopping" => false 
      ),
       "GreenOlives" => array( 
        "title" => "Green Olives", 
        "extratopping" => false 
      ),
       "Onions" => array( 
        "title" => "Onions", 
        "extratopping" => true 
      ),
       "Tomatoes" => array( 
        "title" => "Tomatoes", 
        "extratopping" => true 
      ),
       "Green Pepper," => array( 
        "title" => "Green Pepper", 
        "extratopping" => false 
      ), 
        "Spanich" => array( 
        "title" => "Spanich", 
        "extratopping" => true,
        "extradollar" => true 
      ), 
        "Pineapple" => array( 
        "title" => "Pineapple", 
        "extratopping" => true,
        "extradollar"  => true 
      ), 
        "HotBananaPeppers" => array( 
        "title" => "Hot Banana Peppers", 
        "extratopping" => true,
        "extradollar"  => true 
      ), 
        "Jalapenos" => array( 
        "title" => "Jalapenos", 
        "extratopping" => true,
        "extradollar"  => true 
      ), 
        "Mushroom" => array( 
        "title" => "Mushroom", 
        "extratopping" => true,
        "extradollar"  => true 
      ),
        "GreenChilies" => array( 
        "title" => "Green Chilies", 
        "extratopping" => true,
        "extradollar"  => true 
      ) 
);

$Cheese_topping=array(
  
	    "MozzarellaCheese" => array( 
        "title" => "Mozzarella Cheese", 
        "extratopping" => false 
      ),
       "FetaCheese" => array( 
        "title" => "Feta Cheese", 
        "extratopping" => false 
      )
   );

?>