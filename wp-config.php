<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pizza');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4FCdgU%Ov55Kk{W2no%p`r?>|2w&]#MXM$Y&-_fG,_ch0tUx~>u&28Cz]2Xk4}DC');
define('SECURE_AUTH_KEY',  '07Vu7`5IP(=_Wi!NmAi<D_hCspO=L@fEHN`ln{LN@IgyMBKrA?G&l)T&*T`DLjM{');
define('LOGGED_IN_KEY',    ')aO+&!crraLgD@+jJ9{,ycE@;dZ^eXIi*GbS{8P3) ].2U1[,uQoG8:*12*;Csjq');
define('NONCE_KEY',        'kdlP*O=.]e(]{^sj@Je!Q/XjhOp,su;omGd>#F|GLCyZSmJ&>aghFM U(+J]K|/Z');
define('AUTH_SALT',        '&{oSj0p&`a9tyA[OWYx/F_p@}ey=5Jjw(Fr:l&(/}eJSP3.$kwYVG%bSPV~Mmh1H');
define('SECURE_AUTH_SALT', ']F<0c<=mz-wdEyj!ch^CzdD60y}.g9r$i CX_FpBGySo+3/3HM|J=2K/}k8L9TtD');
define('LOGGED_IN_SALT',   '2Wx*da^N5cQde=9qYtsu(*_[ J(/?bF9/3v#kMpk,Hq0~KW&p%G K3t$z[Izpc7%');
define('NONCE_SALT',       'dK]x62bkWHl5G}-2>B#ut/c|GX4G+ZQ-/g=q,b:x>./T0he*ZWNCoRfaK HbVdTQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
